<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*
Tested working with PHP5.4 and above (including PHP 7 )

 */
require_once 'vendor/autoload.php';

use FormGuide\Handlx\FormHandler;


$pp = new FormHandler(); 

$validator = $pp->getValidator();
$validator->fields(['nome','e-mail','telefone'])->areRequired();
$validator->field('e-mail')->isEmail();
$validator->field('telefone')->maxLength(11);
$validator->field('atendimento');
$validator->field('comentario')->maxLength(6000);



//$pp->sendEmailTo('aniellepizzo@gmail.com');
$pp->sendEmailTo('varandas.pitangueiras@gmail.com'); // ← Your email here
$pp->sendEmailTo('bruna@occa3.com'); // ← Your email here
$pp->sendEmailTo('lead@occa3.com'); // ← Your email here
$pp->sendEmailTo('varandaspitangueiras@maximoaldana.com.br'); // ← Your email here
$pp->sendEmailTo('melissa@agenciacam.com'); // ← Your email here


echo $pp->process($_POST);