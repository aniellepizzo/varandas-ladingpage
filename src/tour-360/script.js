TDV.PlayerAPI.defineScript({ "definitions": [
 {
  "hfov": 360,
  "vfov": 180,
  "adjacentPanoramas": [
   {
    "class": "AdjacentPanorama",
    "panorama": {
     "hfov": 360,
     "vfov": 180,
     "adjacentPanoramas": [
      {
       "class": "AdjacentPanorama",
       "panorama": "this.panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0",
       "yaw": 128.92,
       "distance": 1,
       "backwardYaw": 30.14
      },
      {
       "class": "AdjacentPanorama",
       "panorama": {
        "hfov": 360,
        "vfov": 180,
        "adjacentPanoramas": [
         {
          "class": "AdjacentPanorama",
          "panorama": "this.panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9",
          "yaw": -163.1,
          "distance": 1,
          "backwardYaw": 32.12
         }
        ],
        "hfovMin": 60,
        "id": "panorama_395086A0_34F1_FB6F_4195_6EAEA0406285",
        "frames": [
         {
          "sphere": {
           "levels": [
            {
             "height": 3000,
             "class": "ImageResourceLevel",
             "width": 6000,
             "url": "media/panorama_395086A0_34F1_FB6F_4195_6EAEA0406285_hq.jpeg"
            },
            {
             "height": 2499,
             "class": "ImageResourceLevel",
             "width": 4998,
             "url": "media/panorama_395086A0_34F1_FB6F_4195_6EAEA0406285.jpeg"
            }
           ],
           "class": "ImageResource"
          },
          "class": "SphericPanoramaFrame",
          "overlays": [
           {
            "useHandCursor": true,
            "areas": [
             {
              "class": "HotspotPanoramaOverlayArea",
              "mapColor": "#FF0000",
              "click": "this.startPanoramaWithCamera(this.panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9, this.camera_99C7F133_9686_6B27_41B4_CAAEC2508D94); this.mainPlayList.set('selectedIndex', 2)"
             }
            ],
            "class": "HotspotPanoramaOverlay",
            "id": "overlay_39493969_34F1_69F0_41BC_373563C2B66C",
            "items": [
             {
              "hfov": 29.72,
              "yaw": -163.1,
              "class": "HotspotPanoramaOverlayImage",
              "image": {
               "levels": [
                {
                 "height": 532,
                 "class": "ImageResourceLevel",
                 "width": 495,
                 "url": "media/panorama_395086A0_34F1_FB6F_4195_6EAEA0406285_0_HS_0_0.png"
                }
               ],
               "class": "ImageResource"
              },
              "pitch": 1.95
             }
            ],
            "enabledInCardboard": true,
            "maps": [
             {
              "class": "HotspotPanoramaOverlayMap",
              "hfov": 29.72,
              "yaw": -163.1,
              "image": {
               "levels": [
                {
                 "height": 200,
                 "class": "ImageResourceLevel",
                 "width": 185,
                 "url": "media/panorama_395086A0_34F1_FB6F_4195_6EAEA0406285_0_HS_0_0_0_map.gif"
                }
               ],
               "class": "ImageResource"
              },
              "pitch": 1.95
             }
            ],
            "rollOverDisplay": false
           }
          ],
          "thumbnailUrl": "media/panorama_395086A0_34F1_FB6F_4195_6EAEA0406285_t.jpg"
         }
        ],
        "pitch": 0,
        "hfovMax": 120,
        "label": "banheiro",
        "partial": false,
        "thumbnailUrl": "media/panorama_395086A0_34F1_FB6F_4195_6EAEA0406285_t.jpg",
        "class": "Panorama"
       },
       "yaw": 32.12,
       "distance": 1,
       "backwardYaw": -163.1
      }
     ],
     "hfovMin": 60,
     "id": "panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9",
     "frames": [
      {
       "sphere": {
        "levels": [
         {
          "height": 3000,
          "class": "ImageResourceLevel",
          "width": 6000,
          "url": "media/panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_hq.jpeg"
         },
         {
          "height": 2499,
          "class": "ImageResourceLevel",
          "width": 4998,
          "url": "media/panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9.jpeg"
         }
        ],
        "class": "ImageResource"
       },
       "class": "SphericPanoramaFrame",
       "overlays": [
        {
         "useHandCursor": true,
         "areas": [
          {
           "class": "HotspotPanoramaOverlayArea",
           "mapColor": "#FF0000",
           "click": "this.startPanoramaWithCamera(this.panorama_395086A0_34F1_FB6F_4195_6EAEA0406285, this.camera_99BB70FF_9686_691F_41C0_0A19807ED4C7); this.mainPlayList.set('selectedIndex', 6)"
          }
         ],
         "class": "HotspotPanoramaOverlay",
         "id": "overlay_3835D69A_34F1_9B50_41B7_EA0117680E61",
         "items": [
          {
           "hfov": 18.98,
           "yaw": 32.12,
           "class": "HotspotPanoramaOverlayImage",
           "image": {
            "levels": [
             {
              "height": 287,
              "class": "ImageResourceLevel",
              "width": 316,
              "url": "media/panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_0_HS_0_0.png"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": -1.66
          }
         ],
         "enabledInCardboard": true,
         "maps": [
          {
           "class": "HotspotPanoramaOverlayMap",
           "hfov": 18.98,
           "yaw": 32.12,
           "image": {
            "levels": [
             {
              "height": 143,
              "class": "ImageResourceLevel",
              "width": 158,
              "url": "media/panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_0_HS_0_0_0_map.gif"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": -1.66
          }
         ],
         "rollOverDisplay": false
        },
        {
         "useHandCursor": true,
         "areas": [
          {
           "class": "HotspotPanoramaOverlayArea",
           "mapColor": "#FF0000",
           "click": "this.startPanoramaWithCamera(this.panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0, this.camera_99ACA0EE_9686_6921_41C4_9760B2E3FACB); this.mainPlayList.set('selectedIndex', 0)"
          }
         ],
         "class": "HotspotPanoramaOverlay",
         "id": "overlay_39E60459_34F0_9FD0_41B1_A0F40481F001",
         "items": [
          {
           "hfov": 24.76,
           "yaw": 128.92,
           "class": "HotspotPanoramaOverlayImage",
           "image": {
            "levels": [
             {
              "height": 392,
              "class": "ImageResourceLevel",
              "width": 413,
              "url": "media/panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_0_HS_1_0.png"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": -2.94
          }
         ],
         "enabledInCardboard": true,
         "maps": [
          {
           "class": "HotspotPanoramaOverlayMap",
           "hfov": 24.76,
           "yaw": 128.92,
           "image": {
            "levels": [
             {
              "height": 190,
              "class": "ImageResourceLevel",
              "width": 200,
              "url": "media/panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_0_HS_1_0_0_map.gif"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": -2.94
          }
         ],
         "rollOverDisplay": false
        }
       ],
       "thumbnailUrl": "media/panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_t.jpg"
      }
     ],
     "pitch": 0,
     "hfovMax": 120,
     "label": "suite",
     "partial": false,
     "thumbnailUrl": "media/panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_t.jpg",
     "class": "Panorama"
    },
    "yaw": 30.14,
    "distance": 1,
    "backwardYaw": 128.92
   },
   {
    "class": "AdjacentPanorama",
    "panorama": {
     "hfov": 360,
     "vfov": 180,
     "adjacentPanoramas": [
      {
       "class": "AdjacentPanorama",
       "panorama": "this.panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0",
       "yaw": 140.6,
       "distance": 1,
       "backwardYaw": 126.93
      },
      {
       "class": "AdjacentPanorama",
       "panorama": {
        "hfov": 360,
        "vfov": 180,
        "adjacentPanoramas": [
         {
          "class": "AdjacentPanorama",
          "panorama": "this.panorama_3AFD8338_34F0_B950_41AD_70C689AED525",
          "yaw": -97.89,
          "distance": 1,
          "backwardYaw": 43.95
         },
         {
          "class": "AdjacentPanorama",
          "panorama": "this.panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0",
          "yaw": 141.27,
          "distance": 1,
          "backwardYaw": -97.13
         }
        ],
        "hfovMin": 60,
        "id": "panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861",
        "frames": [
         {
          "sphere": {
           "levels": [
            {
             "height": 3000,
             "class": "ImageResourceLevel",
             "width": 6000,
             "url": "media/panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_hq.jpeg"
            },
            {
             "height": 2499,
             "class": "ImageResourceLevel",
             "width": 4998,
             "url": "media/panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861.jpeg"
            }
           ],
           "class": "ImageResource"
          },
          "class": "SphericPanoramaFrame",
          "overlays": [
           {
            "useHandCursor": true,
            "areas": [
             {
              "class": "HotspotPanoramaOverlayArea",
              "mapColor": "#FF0000",
              "click": "this.startPanoramaWithCamera(this.panorama_3AFD8338_34F0_B950_41AD_70C689AED525, this.camera_9938B1BF_9686_6B1F_41DF_A7F77125E6DC); this.mainPlayList.set('selectedIndex', 4)"
             }
            ],
            "class": "HotspotPanoramaOverlay",
            "id": "overlay_39E42BA9_34FF_A970_4192_1F619C0E9E18",
            "items": [
             {
              "hfov": 22.46,
              "yaw": -97.89,
              "class": "HotspotPanoramaOverlayImage",
              "image": {
               "levels": [
                {
                 "height": 409,
                 "class": "ImageResourceLevel",
                 "width": 374,
                 "url": "media/panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_0_HS_0_0.png"
                }
               ],
               "class": "ImageResource"
              },
              "pitch": -1.38
             }
            ],
            "enabledInCardboard": true,
            "maps": [
             {
              "class": "HotspotPanoramaOverlayMap",
              "hfov": 22.46,
              "yaw": -97.89,
              "image": {
               "levels": [
                {
                 "height": 200,
                 "class": "ImageResourceLevel",
                 "width": 183,
                 "url": "media/panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_0_HS_0_0_0_map.gif"
                }
               ],
               "class": "ImageResource"
              },
              "pitch": -1.38
             }
            ],
            "rollOverDisplay": false
           },
           {
            "useHandCursor": true,
            "areas": [
             {
              "class": "HotspotPanoramaOverlayArea",
              "mapColor": "#FF0000",
              "click": "this.startPanoramaWithCamera(this.panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0, this.camera_994AD1DE_9686_6B61_418A_957818527E53); this.mainPlayList.set('selectedIndex', 0)"
             }
            ],
            "class": "HotspotPanoramaOverlay",
            "id": "overlay_39DABC92_34F0_AF53_41B4_1442B06D6B6C",
            "items": [
             {
              "hfov": 24.37,
              "yaw": 141.27,
              "class": "HotspotPanoramaOverlayImage",
              "image": {
               "levels": [
                {
                 "height": 406,
                 "class": "ImageResourceLevel",
                 "width": 406,
                 "url": "media/panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_0_HS_1_0.png"
                }
               ],
               "class": "ImageResource"
              },
              "pitch": 3.02
             }
            ],
            "enabledInCardboard": true,
            "maps": [
             {
              "class": "HotspotPanoramaOverlayMap",
              "hfov": 24.37,
              "yaw": 141.27,
              "image": {
               "levels": [
                {
                 "height": 200,
                 "class": "ImageResourceLevel",
                 "width": 200,
                 "url": "media/panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_0_HS_1_0_0_map.gif"
                }
               ],
               "class": "ImageResource"
              },
              "pitch": 3.02
             }
            ],
            "rollOverDisplay": false
           }
          ],
          "thumbnailUrl": "media/panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_t.jpg"
         }
        ],
        "pitch": 0,
        "hfovMax": 120,
        "label": "terraco",
        "partial": false,
        "thumbnailUrl": "media/panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_t.jpg",
        "class": "Panorama"
       },
       "yaw": 43.95,
       "distance": 1,
       "backwardYaw": -97.89
      }
     ],
     "hfovMin": 60,
     "id": "panorama_3AFD8338_34F0_B950_41AD_70C689AED525",
     "frames": [
      {
       "sphere": {
        "levels": [
         {
          "height": 3000,
          "class": "ImageResourceLevel",
          "width": 6000,
          "url": "media/panorama_3AFD8338_34F0_B950_41AD_70C689AED525_hq.jpeg"
         },
         {
          "height": 2499,
          "class": "ImageResourceLevel",
          "width": 4998,
          "url": "media/panorama_3AFD8338_34F0_B950_41AD_70C689AED525.jpeg"
         }
        ],
        "class": "ImageResource"
       },
       "class": "SphericPanoramaFrame",
       "overlays": [
        {
         "useHandCursor": true,
         "areas": [
          {
           "class": "HotspotPanoramaOverlayArea",
           "mapColor": "#FF0000",
           "click": "this.startPanoramaWithCamera(this.panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0, this.camera_99B28110_9686_68E1_41B0_247212037EBB); this.mainPlayList.set('selectedIndex', 0)"
          }
         ],
         "class": "HotspotPanoramaOverlay",
         "id": "overlay_39C3588A_34F1_B733_41C5_94E4B98563B5",
         "items": [
          {
           "hfov": 26.77,
           "yaw": 140.6,
           "class": "HotspotPanoramaOverlayImage",
           "image": {
            "levels": [
             {
              "height": 516,
              "class": "ImageResourceLevel",
              "width": 447,
              "url": "media/panorama_3AFD8338_34F0_B950_41AD_70C689AED525_0_HS_0_0.png"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": 4.91
          }
         ],
         "enabledInCardboard": true,
         "maps": [
          {
           "class": "HotspotPanoramaOverlayMap",
           "hfov": 26.77,
           "yaw": 140.6,
           "image": {
            "levels": [
             {
              "height": 200,
              "class": "ImageResourceLevel",
              "width": 172,
              "url": "media/panorama_3AFD8338_34F0_B950_41AD_70C689AED525_0_HS_0_0_0_map.gif"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": 4.91
          }
         ],
         "rollOverDisplay": false
        },
        {
         "useHandCursor": true,
         "areas": [
          {
           "class": "HotspotPanoramaOverlayArea",
           "mapColor": "#FF0000",
           "click": "this.startPanoramaWithCamera(this.panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861, this.camera_99C8F122_9686_6B21_41DD_AB3FADBA6E15); this.mainPlayList.set('selectedIndex', 3)"
          }
         ],
         "class": "HotspotPanoramaOverlay",
         "id": "overlay_394824B1_34F1_BF50_41BC_1FE8AE0DAF63",
         "items": [
          {
           "hfov": 24.27,
           "yaw": 43.95,
           "class": "HotspotPanoramaOverlayImage",
           "image": {
            "levels": [
             {
              "height": 379,
              "class": "ImageResourceLevel",
              "width": 406,
              "url": "media/panorama_3AFD8338_34F0_B950_41AD_70C689AED525_0_HS_1_0.png"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": 6.14
          }
         ],
         "enabledInCardboard": true,
         "maps": [
          {
           "class": "HotspotPanoramaOverlayMap",
           "hfov": 24.27,
           "yaw": 43.95,
           "image": {
            "levels": [
             {
              "height": 186,
              "class": "ImageResourceLevel",
              "width": 200,
              "url": "media/panorama_3AFD8338_34F0_B950_41AD_70C689AED525_0_HS_1_0_0_map.gif"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": 6.14
          }
         ],
         "rollOverDisplay": false
        }
       ],
       "thumbnailUrl": "media/panorama_3AFD8338_34F0_B950_41AD_70C689AED525_t.jpg"
      }
     ],
     "pitch": 0,
     "hfovMax": 120,
     "label": "cozinha",
     "partial": false,
     "thumbnailUrl": "media/panorama_3AFD8338_34F0_B950_41AD_70C689AED525_t.jpg",
     "class": "Panorama"
    },
    "yaw": 126.93,
    "distance": 1,
    "backwardYaw": 140.6
   },
   {
    "class": "AdjacentPanorama",
    "panorama": "this.panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861",
    "yaw": -97.13,
    "distance": 1,
    "backwardYaw": 141.27
   }
  ],
  "hfovMin": 60,
  "id": "panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0",
  "frames": [
   {
    "sphere": {
     "levels": [
      {
       "height": 3000,
       "class": "ImageResourceLevel",
       "width": 6000,
       "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_hq.jpeg"
      },
      {
       "height": 2499,
       "class": "ImageResourceLevel",
       "width": 4998,
       "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0.jpeg"
      }
     ],
     "class": "ImageResource"
    },
    "class": "SphericPanoramaFrame",
    "overlays": [
     {
      "useHandCursor": true,
      "areas": [
       {
        "class": "HotspotPanoramaOverlayArea",
        "mapColor": "#FF0000",
        "click": "this.mainPlayList.set('selectedIndex', 1)"
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "id": "overlay_3F20FD50_34F1_A9D0_419B_73AF0D335570",
      "items": [
       {
        "hfov": 12.92,
        "yaw": 83.04,
        "class": "HotspotPanoramaOverlayImage",
        "image": {
         "levels": [
          {
           "height": 214,
           "class": "ImageResourceLevel",
           "width": 215,
           "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_0_HS_0_0.png"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.96
       }
      ],
      "enabledInCardboard": true,
      "maps": [
       {
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 12.92,
        "yaw": 83.04,
        "image": {
         "levels": [
          {
           "height": 107,
           "class": "ImageResourceLevel",
           "width": 107,
           "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_0_HS_0_0_0_map.gif"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.96
       }
      ],
      "rollOverDisplay": false
     },
     {
      "useHandCursor": true,
      "areas": [
       {
        "class": "HotspotPanoramaOverlayArea",
        "mapColor": "#FF0000",
        "click": "this.startPanoramaWithCamera(this.panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9, this.camera_9901918A_9686_6BE1_41E1_D78B8DF26606); this.mainPlayList.set('selectedIndex', 2)"
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "id": "overlay_38DA17D2_34F7_78D0_41C2_1AD6C56DEAE4",
      "items": [
       {
        "hfov": 14.49,
        "yaw": 30.14,
        "class": "HotspotPanoramaOverlayImage",
        "image": {
         "levels": [
          {
           "height": 227,
           "class": "ImageResourceLevel",
           "width": 241,
           "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_0_HS_1_0.png"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.56
       }
      ],
      "enabledInCardboard": true,
      "maps": [
       {
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 14.49,
        "yaw": 30.14,
        "image": {
         "levels": [
          {
           "height": 113,
           "class": "ImageResourceLevel",
           "width": 120,
           "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_0_HS_1_0_0_map.gif"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.56
       }
      ],
      "rollOverDisplay": false
     },
     {
      "useHandCursor": true,
      "areas": [
       {
        "class": "HotspotPanoramaOverlayArea",
        "mapColor": "#FF0000",
        "click": "this.startPanoramaWithCamera(this.panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861, this.camera_992F71AD_9686_6B23_41BE_CB6BB0D17C85); this.mainPlayList.set('selectedIndex', 3)"
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "id": "overlay_38695A59_34F0_ABD0_41C2_3D4680DD80BF",
      "items": [
       {
        "hfov": 16.65,
        "yaw": -97.13,
        "class": "HotspotPanoramaOverlayImage",
        "image": {
         "levels": [
          {
           "height": 277,
           "class": "ImageResourceLevel",
           "width": 277,
           "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_0_HS_2_0.png"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.06
       }
      ],
      "enabledInCardboard": true,
      "maps": [
       {
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 16.65,
        "yaw": -97.13,
        "image": {
         "levels": [
          {
           "height": 138,
           "class": "ImageResourceLevel",
           "width": 138,
           "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_0_HS_2_0_0_map.gif"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.06
       }
      ],
      "rollOverDisplay": false
     },
     {
      "useHandCursor": true,
      "areas": [
       {
        "class": "HotspotPanoramaOverlayArea",
        "mapColor": "#FF0000",
        "click": "this.startPanoramaWithCamera(this.panorama_3AFD8338_34F0_B950_41AD_70C689AED525, this.camera_9913A19C_9686_6BE1_41CD_118F176A8A3E); this.mainPlayList.set('selectedIndex', 4)"
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "id": "overlay_3886E2A2_34F1_FB73_4187_6AD259493093",
      "items": [
       {
        "hfov": 15.65,
        "yaw": 126.93,
        "class": "HotspotPanoramaOverlayImage",
        "image": {
         "levels": [
          {
           "height": 221,
           "class": "ImageResourceLevel",
           "width": 260,
           "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_0_HS_3_0.png"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.86
       }
      ],
      "enabledInCardboard": true,
      "maps": [
       {
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 15.65,
        "yaw": 126.93,
        "image": {
         "levels": [
          {
           "height": 110,
           "class": "ImageResourceLevel",
           "width": 130,
           "url": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_0_HS_3_0_0_map.gif"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.86
       }
      ],
      "rollOverDisplay": false
     }
    ],
    "thumbnailUrl": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_t.jpg"
   }
  ],
  "pitch": 0,
  "hfovMax": 120,
  "label": "jantarestar",
  "partial": false,
  "thumbnailUrl": "media/panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_t.jpg",
  "class": "Panorama"
 },
 {
  "class": "PanoramaPlayer",
  "displayPlaybackBar": true,
  "id": "MainViewerPanoramaPlayer",
  "gyroscopeVerticalDraggingEnabled": true,
  "touchControlMode": "drag_rotation",
  "buttonCardboardView": "this.IconButton_3B2A622B_34F3_FB70_41A6_C18F4175D785",
  "mouseControlMode": "drag_acceleration",
  "viewerArea": "this.MainViewer",
  "preloadEnabled": false
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "hfov": 120,
   "yaw": -20.89,
   "pitch": -5.73
  },
  "class": "PanoramaCamera",
  "id": "panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "hfov": 360,
  "vfov": 180,
  "hfovMin": 60,
  "id": "panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8",
  "frames": [
   {
    "sphere": {
     "levels": [
      {
       "height": 3000,
       "class": "ImageResourceLevel",
       "width": 6000,
       "url": "media/panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8_hq.jpeg"
      },
      {
       "height": 2499,
       "class": "ImageResourceLevel",
       "width": 4998,
       "url": "media/panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8.jpeg"
      }
     ],
     "class": "ImageResource"
    },
    "class": "SphericPanoramaFrame",
    "overlays": [
     {
      "useHandCursor": true,
      "areas": [
       {
        "class": "HotspotPanoramaOverlayArea",
        "mapColor": "#FF0000",
        "click": "this.mainPlayList.set('selectedIndex', 5)"
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "id": "overlay_383C7131_34F3_9950_41BD_B425D902841F",
      "items": [
       {
        "hfov": 18.19,
        "yaw": 113.94,
        "class": "HotspotPanoramaOverlayImage",
        "image": {
         "levels": [
          {
           "height": 344,
           "class": "ImageResourceLevel",
           "width": 303,
           "url": "media/panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8_0_HS_0_0.png"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": 0.44
       }
      ],
      "enabledInCardboard": true,
      "maps": [
       {
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 18.19,
        "yaw": 113.94,
        "image": {
         "levels": [
          {
           "height": 172,
           "class": "ImageResourceLevel",
           "width": 151,
           "url": "media/panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8_0_HS_0_0_0_map.gif"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": 0.44
       }
      ],
      "rollOverDisplay": false
     }
    ],
    "thumbnailUrl": "media/panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8_t.jpg"
   }
  ],
  "pitch": 0,
  "hfovMax": 120,
  "label": "gourmet",
  "partial": false,
  "thumbnailUrl": "media/panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8_t.jpg",
  "class": "Panorama"
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 0,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 "this.panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9",
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 0,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 "this.panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861",
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 0,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 "this.panorama_3AFD8338_34F0_B950_41AD_70C689AED525",
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 0,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "panorama_3AFD8338_34F0_B950_41AD_70C689AED525_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "hfov": 360,
  "vfov": 180,
  "adjacentPanoramas": [
   {
    "class": "AdjacentPanorama",
    "panorama": {
     "hfov": 360,
     "vfov": 180,
     "adjacentPanoramas": [
      {
       "class": "AdjacentPanorama",
       "panorama": "this.panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9",
       "yaw": -76.26,
       "distance": 1,
       "backwardYaw": 68.88
      }
     ],
     "hfovMin": 60,
     "id": "panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933",
     "frames": [
      {
       "sphere": {
        "levels": [
         {
          "height": 3000,
          "class": "ImageResourceLevel",
          "width": 6000,
          "url": "media/panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_hq.jpeg"
         },
         {
          "height": 2499,
          "class": "ImageResourceLevel",
          "width": 4998,
          "url": "media/panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933.jpeg"
         }
        ],
        "class": "ImageResource"
       },
       "class": "SphericPanoramaFrame",
       "overlays": [
        {
         "useHandCursor": true,
         "areas": [
          {
           "class": "HotspotPanoramaOverlayArea",
           "mapColor": "#FF0000",
           "click": "this.startPanoramaWithCamera(this.panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9, this.camera_99F6A178_9686_6B21_41C9_F723DB63F130); this.mainPlayList.set('selectedIndex', 5)"
          }
         ],
         "class": "HotspotPanoramaOverlay",
         "id": "overlay_392B3642_34F1_9B33_41AF_D27250194BF4",
         "items": [
          {
           "hfov": 23.11,
           "yaw": -76.26,
           "class": "HotspotPanoramaOverlayImage",
           "image": {
            "levels": [
             {
              "height": 416,
              "class": "ImageResourceLevel",
              "width": 386,
              "url": "media/panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_0_HS_0_0.png"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": 4.6
          }
         ],
         "enabledInCardboard": true,
         "maps": [
          {
           "class": "HotspotPanoramaOverlayMap",
           "hfov": 23.11,
           "yaw": -76.26,
           "image": {
            "levels": [
             {
              "height": 200,
              "class": "ImageResourceLevel",
              "width": 185,
              "url": "media/panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_0_HS_0_0_0_map.gif"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": 4.6
          }
         ],
         "rollOverDisplay": false
        },
        {
         "useHandCursor": true,
         "areas": [
          {
           "class": "HotspotPanoramaOverlayArea",
           "mapColor": "#FF0000",
           "click": "this.mainPlayList.set('selectedIndex', 8)"
          }
         ],
         "class": "HotspotPanoramaOverlay",
         "id": "overlay_3AA512E1_34F0_F8F0_4194_3C34A8B2733C",
         "items": [
          {
           "hfov": 26.64,
           "yaw": -153.89,
           "class": "HotspotPanoramaOverlayImage",
           "image": {
            "levels": [
             {
              "height": 427,
              "class": "ImageResourceLevel",
              "width": 447,
              "url": "media/panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_0_HS_1_0.png"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": 7.55
          }
         ],
         "enabledInCardboard": true,
         "maps": [
          {
           "class": "HotspotPanoramaOverlayMap",
           "hfov": 26.64,
           "yaw": -153.89,
           "image": {
            "levels": [
             {
              "height": 191,
              "class": "ImageResourceLevel",
              "width": 200,
              "url": "media/panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_0_HS_1_0_0_map.gif"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": 7.55
          }
         ],
         "rollOverDisplay": false
        }
       ],
       "thumbnailUrl": "media/panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_t.jpg"
      }
     ],
     "pitch": 0,
     "hfovMax": 120,
     "label": "sinuca_02",
     "partial": false,
     "thumbnailUrl": "media/panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_t.jpg",
     "class": "Panorama"
    },
    "yaw": 68.88,
    "distance": 1,
    "backwardYaw": -76.26
   },
   {
    "class": "AdjacentPanorama",
    "panorama": {
     "hfov": 360,
     "vfov": 180,
     "adjacentPanoramas": [
      {
       "class": "AdjacentPanorama",
       "panorama": "this.panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9",
       "yaw": -39.26,
       "distance": 1,
       "backwardYaw": 112.11
      }
     ],
     "hfovMin": 60,
     "id": "panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C",
     "frames": [
      {
       "sphere": {
        "levels": [
         {
          "height": 3000,
          "class": "ImageResourceLevel",
          "width": 6000,
          "url": "media/panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_hq.jpeg"
         },
         {
          "height": 2499,
          "class": "ImageResourceLevel",
          "width": 4998,
          "url": "media/panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C.jpeg"
         }
        ],
        "class": "ImageResource"
       },
       "class": "SphericPanoramaFrame",
       "overlays": [
        {
         "useHandCursor": true,
         "areas": [
          {
           "class": "HotspotPanoramaOverlayArea",
           "mapColor": "#FF0000",
           "click": "this.startPanoramaWithCamera(this.panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9, this.camera_99E49167_9686_6B2F_41D8_655E2C565D92); this.mainPlayList.set('selectedIndex', 5)"
          }
         ],
         "class": "HotspotPanoramaOverlay",
         "id": "overlay_3AC927EA_34F7_98F0_41CA_415819B300BA",
         "items": [
          {
           "hfov": 24.56,
           "yaw": -39.26,
           "class": "HotspotPanoramaOverlayImage",
           "image": {
            "levels": [
             {
              "height": 458,
              "class": "ImageResourceLevel",
              "width": 409,
              "url": "media/panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_0_HS_0_0.png"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": -1.16
          }
         ],
         "enabledInCardboard": true,
         "maps": [
          {
           "class": "HotspotPanoramaOverlayMap",
           "hfov": 24.56,
           "yaw": -39.26,
           "image": {
            "levels": [
             {
              "height": 200,
              "class": "ImageResourceLevel",
              "width": 178,
              "url": "media/panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_0_HS_0_0_0_map.gif"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": -1.16
          }
         ],
         "rollOverDisplay": false
        },
        {
         "useHandCursor": true,
         "areas": [
          {
           "class": "HotspotPanoramaOverlayArea",
           "mapColor": "#FF0000",
           "click": "this.mainPlayList.set('selectedIndex', 0)"
          }
         ],
         "class": "HotspotPanoramaOverlay",
         "id": "overlay_3AB530E1_34F0_98F0_41BD_B89B0DF0C3A5",
         "items": [
          {
           "hfov": 26.24,
           "yaw": -116.69,
           "class": "HotspotPanoramaOverlayImage",
           "image": {
            "levels": [
             {
              "height": 420,
              "class": "ImageResourceLevel",
              "width": 439,
              "url": "media/panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_0_HS_1_0.png"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": -5.45
          }
         ],
         "enabledInCardboard": true,
         "maps": [
          {
           "class": "HotspotPanoramaOverlayMap",
           "hfov": 26.24,
           "yaw": -116.69,
           "image": {
            "levels": [
             {
              "height": 191,
              "class": "ImageResourceLevel",
              "width": 200,
              "url": "media/panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_0_HS_1_0_0_map.gif"
             }
            ],
            "class": "ImageResource"
           },
           "pitch": -5.45
          }
         ],
         "rollOverDisplay": false
        }
       ],
       "thumbnailUrl": "media/panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_t.jpg"
      }
     ],
     "pitch": 0,
     "hfovMax": 120,
     "label": "piscina",
     "partial": false,
     "thumbnailUrl": "media/panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_t.jpg",
     "class": "Panorama"
    },
    "yaw": 112.11,
    "distance": 1,
    "backwardYaw": -39.26
   }
  ],
  "hfovMin": 60,
  "id": "panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9",
  "frames": [
   {
    "sphere": {
     "levels": [
      {
       "height": 3000,
       "class": "ImageResourceLevel",
       "width": 6000,
       "url": "media/panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_hq.jpeg"
      },
      {
       "height": 2499,
       "class": "ImageResourceLevel",
       "width": 4998,
       "url": "media/panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9.jpeg"
      }
     ],
     "class": "ImageResource"
    },
    "class": "SphericPanoramaFrame",
    "overlays": [
     {
      "useHandCursor": true,
      "areas": [
       {
        "class": "HotspotPanoramaOverlayArea",
        "mapColor": "#FF0000",
        "click": "this.startPanoramaWithCamera(this.panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933, this.camera_99DD5144_9686_6B61_41D5_F1B5560CCB7C); this.mainPlayList.set('selectedIndex', 7)"
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "id": "overlay_39621611_34F0_9B53_418C_59DA1ADB193A",
      "items": [
       {
        "hfov": 19.71,
        "yaw": 68.88,
        "class": "HotspotPanoramaOverlayImage",
        "image": {
         "levels": [
          {
           "height": 394,
           "class": "ImageResourceLevel",
           "width": 328,
           "url": "media/panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_0_HS_0_0.png"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": 1.18
       }
      ],
      "enabledInCardboard": true,
      "maps": [
       {
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 19.71,
        "yaw": 68.88,
        "image": {
         "levels": [
          {
           "height": 197,
           "class": "ImageResourceLevel",
           "width": 164,
           "url": "media/panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_0_HS_0_0_0_map.gif"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": 1.18
       }
      ],
      "rollOverDisplay": false
     },
     {
      "useHandCursor": true,
      "areas": [
       {
        "class": "HotspotPanoramaOverlayArea",
        "mapColor": "#FF0000",
        "click": "this.startPanoramaWithCamera(this.panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C, this.camera_99E98155_9686_6B62_41D9_9A507DE5EE7C); this.mainPlayList.set('selectedIndex', 8)"
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "id": "overlay_3959D27A_34F3_BBD3_4192_92E1CF3095C5",
      "items": [
       {
        "hfov": 24.63,
        "yaw": 112.11,
        "class": "HotspotPanoramaOverlayImage",
        "image": {
         "levels": [
          {
           "height": 392,
           "class": "ImageResourceLevel",
           "width": 410,
           "url": "media/panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_0_HS_1_0.png"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": 0.12
       }
      ],
      "enabledInCardboard": true,
      "maps": [
       {
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 24.63,
        "yaw": 112.11,
        "image": {
         "levels": [
          {
           "height": 191,
           "class": "ImageResourceLevel",
           "width": 200,
           "url": "media/panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_0_HS_1_0_0_map.gif"
          }
         ],
         "class": "ImageResource"
        },
        "pitch": 0.12
       }
      ],
      "rollOverDisplay": false
     }
    ],
    "thumbnailUrl": "media/panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_t.jpg"
   }
  ],
  "pitch": 0,
  "hfovMax": 120,
  "label": "sinuca_01",
  "partial": false,
  "thumbnailUrl": "media/panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_t.jpg",
  "class": "Panorama"
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 0,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 "this.panorama_395086A0_34F1_FB6F_4195_6EAEA0406285",
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 0,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "panorama_395086A0_34F1_FB6F_4195_6EAEA0406285_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 "this.panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933",
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 0,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 "this.panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C",
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 0,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_camera",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "class": "PlayList",
  "id": "mainPlayList",
  "items": [
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 0, 1)",
    "camera": "this.panorama_3FC09D60_34F1_A9F0_41C3_3321D2890CC0_camera"
   },
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 1, 2)",
    "camera": "this.panorama_39C00265_34F0_BBF0_41C2_1E7C383F20E8_camera"
   },
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 2, 3)",
    "camera": "this.panorama_39AB1E45_34F7_EB30_4184_C80AC372E1B9_camera"
   },
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 3, 4)",
    "camera": "this.panorama_3964F262_34F0_BBF0_41C5_1A976F7B5861_camera"
   },
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_3AFD8338_34F0_B950_41AD_70C689AED525",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 4, 5)",
    "camera": "this.panorama_3AFD8338_34F0_B950_41AD_70C689AED525_camera"
   },
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 5, 6)",
    "camera": "this.panorama_390C0575_34F3_79D0_41BB_0878D35DD0A9_camera"
   },
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_395086A0_34F1_FB6F_4195_6EAEA0406285",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 6, 7)",
    "camera": "this.panorama_395086A0_34F1_FB6F_4195_6EAEA0406285_camera"
   },
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 7, 8)",
    "camera": "this.panorama_3A42885F_34F3_97D1_41C6_6F0E6B508933_camera"
   },
   {
    "class": "PanoramaPlayListItem",
    "player": "this.MainViewerPanoramaPlayer",
    "media": "this.panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 8, 0)",
    "camera": "this.panorama_3A5C0264_34F0_BBF0_41C6_8E8903F5FF7C_camera"
   }
  ]
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "hfov": 120,
   "yaw": -149.86,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99ACA0EE_9686_6921_41C4_9760B2E3FACB",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 16.9,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99BB70FF_9686_691F_41C0_0A19807ED4C7",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "hfov": 120,
   "yaw": -53.07,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99B28110_9686_68E1_41B0_247212037EBB",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 82.11,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99C8F122_9686_6B21_41DD_AB3FADBA6E15",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": -147.88,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99C7F133_9686_6B27_41B4_CAAEC2508D94",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 103.74,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99DD5144_9686_6B61_41D5_F1B5560CCB7C",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": 140.74,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99E98155_9686_6B62_41D9_9A507DE5EE7C",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": -67.89,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99E49167_9686_6B2F_41D8_655E2C565D92",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": -111.12,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_99F6A178_9686_6B21_41C9_F723DB63F130",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": -51.08,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_9901918A_9686_6BE1_41E1_D78B8DF26606",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": -39.4,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_9913A19C_9686_6BE1_41CD_118F176A8A3E",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": -38.73,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_992F71AD_9686_6B23_41BE_CB6BB0D17C85",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "yaw": -136.05,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_9938B1BF_9686_6B1F_41DF_A7F77125E6DC",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 },
 {
  "initialPosition": {
   "class": "PanoramaCameraPosition",
   "hfov": 120,
   "yaw": 82.87,
   "pitch": 0
  },
  "class": "PanoramaCamera",
  "id": "camera_994AD1DE_9686_6B61_418A_957818527E53",
  "automaticRotationSpeed": 20,
  "automaticZoomSpeed": 10,
  "initialSequence": {
   "movements": [
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "yawSpeed": 7.96,
     "class": "DistancePanoramaCameraMovement",
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "class": "PanoramaCameraSequence",
   "restartMovementOnUserInteraction": false
  }
 }
], "children": [
 {
  "playbackBarOpacity": 1,
  "playbackBarHeadBorderSize": 0,
  "toolTipBorderColor": "#767676",
  "playbackBarHeadBorderColor": "#000000",
  "playbackBarHeadShadowBlurRadius": 3,
  "shadow": false,
  "toolTipPaddingLeft": 6,
  "paddingTop": 0,
  "toolTipShadowOpacity": 1,
  "playbackBarHeadHeight": 15,
  "playbackBarBorderSize": 0,
  "toolTipTextShadowBlurRadius": 3,
  "borderRadius": 0,
  "playbackBarLeft": 0,
  "playbackBarHeadShadowVerticalLength": 0,
  "playbackBarHeadBackgroundColorRatios": [
   0,
   1
  ],
  "toolTipTextShadowColor": "#000000",
  "progressBarBackgroundColorRatios": [
   0
  ],
  "progressBarBorderColor": "#000000",
  "playbackBarHeadShadowColor": "#000000",
  "toolTipBorderSize": 1,
  "minHeight": 50,
  "paddingLeft": 0,
  "toolTipFontFamily": "Arial",
  "progressBackgroundColorDirection": "vertical",
  "progressBarOpacity": 1,
  "progressBorderColor": "#000000",
  "progressBarBackgroundColor": [
   "#3399FF"
  ],
  "playbackBarBackgroundOpacity": 1,
  "progressBottom": 0,
  "playbackBarBottom": 5,
  "progressOpacity": 1,
  "progressBackgroundOpacity": 1,
  "toolTipBorderRadius": 3,
  "playbackBarProgressBackgroundColorDirection": "vertical",
  "borderSize": 0,
  "toolTipBackgroundColor": "#F6F6F6",
  "toolTipTextShadowOpacity": 0,
  "playbackBarHeadOpacity": 1,
  "toolTipPaddingBottom": 4,
  "progressBorderRadius": 0,
  "progressBackgroundColor": [
   "#FFFFFF"
  ],
  "playbackBarBackgroundColor": [
   "#FFFFFF"
  ],
  "progressBorderSize": 0,
  "playbackBarHeight": 10,
  "toolTipOpacity": 1,
  "minWidth": 100,
  "playbackBarHeadBackgroundColorDirection": "vertical",
  "playbackBarProgressBorderSize": 0,
  "playbackBarHeadWidth": 6,
  "playbackBarProgressBackgroundColor": [
   "#3399FF"
  ],
  "playbackBarBackgroundColorDirection": "vertical",
  "width": "100%",
  "progressBarBackgroundColorDirection": "vertical",
  "playbackBarRight": 0,
  "progressBarBorderRadius": 0,
  "height": "100%",
  "progressBarBorderSize": 0,
  "toolTipPaddingTop": 4,
  "playbackBarProgressBorderRadius": 0,
  "transitionMode": "blending",
  "playbackBarBorderColor": "#FFFFFF",
  "toolTipShadowColor": "#333333",
  "toolTipShadowBlurRadius": 3,
  "toolTipShadowSpread": 0,
  "playbackBarHeadShadow": true,
  "toolTipFontSize": 12,
  "paddingRight": 0,
  "toolTipFontColor": "#606060",
  "paddingBottom": 0,
  "playbackBarHeadBackgroundColor": [
   "#111111",
   "#666666"
  ],
  "playbackBarBorderRadius": 0,
  "toolTipShadowVerticalLength": 0,
  "toolTipFontWeight": "normal",
  "playbackBarHeadShadowOpacity": 0.7,
  "playbackBarProgressBorderColor": "#000000",
  "id": "MainViewer",
  "progressRight": 0,
  "toolTipShadowHorizontalLength": 0,
  "toolTipPaddingRight": 6,
  "playbackBarHeadBorderRadius": 0,
  "class": "ViewerArea",
  "toolTipFontStyle": "normal",
  "playbackBarHeadShadowHorizontalLength": 0,
  "progressLeft": 0,
  "progressHeight": 10,
  "progressBackgroundColorRatios": [
   0
  ],
  "playbackBarProgressOpacity": 1,
  "playbackBarProgressBackgroundColorRatios": [
   0
  ]
 },
 {
  "verticalAlign": "middle",
  "toolTipBorderColor": "#767676",
  "toolTipPaddingLeft": 6,
  "width": 47,
  "height": 35,
  "toolTipShadowOpacity": 1,
  "maxWidth": 128,
  "iconURL": "skin/IconButton_3B148E12_34F0_AB50_41C3_18F7168B2FBA.png",
  "toolTipTextShadowBlurRadius": 3,
  "borderRadius": 0,
  "horizontalAlign": "center",
  "mode": "toggle",
  "toolTipPaddingTop": 4,
  "toolTipTextShadowColor": "#000000",
  "toolTipShadowSpread": 0,
  "toolTipShadowColor": "#333333",
  "toolTipShadowBlurRadius": 3,
  "toolTipBorderSize": 1,
  "toolTipFontSize": 12,
  "minHeight": 1,
  "toolTipFontColor": "#606060",
  "transparencyActive": true,
  "paddingLeft": 0,
  "toolTipFontFamily": "Arial",
  "cursor": "hand",
  "paddingBottom": 0,
  "paddingRight": 0,
  "backgroundOpacity": 0,
  "toolTip": "Fullscreen",
  "bottom": "8.88%",
  "id": "IconButton_3B148E12_34F0_AB50_41C3_18F7168B2FBA",
  "maxHeight": 128,
  "toolTipShadowVerticalLength": 0,
  "borderSize": 0,
  "toolTipShadowHorizontalLength": 0,
  "toolTipBorderRadius": 3,
  "toolTipPaddingRight": 6,
  "toolTipFontWeight": "normal",
  "toolTipBackgroundColor": "#F6F6F6",
  "toolTipPaddingBottom": 4,
  "toolTipFontStyle": "normal",
  "toolTipTextShadowOpacity": 0,
  "class": "IconButton",
  "minWidth": 1,
  "right": "4.89%",
  "toolTipOpacity": 1,
  "paddingTop": 0,
  "shadow": false
 },
 {
  "verticalAlign": "middle",
  "width": 56,
  "height": 55,
  "maxWidth": 56,
  "iconURL": "skin/IconButton_3B2A622B_34F3_FB70_41A6_C18F4175D785.png",
  "borderRadius": 0,
  "horizontalAlign": "center",
  "mode": "push",
  "minHeight": 1,
  "transparencyActive": false,
  "paddingLeft": 0,
  "cursor": "hand",
  "paddingBottom": 0,
  "paddingRight": 0,
  "backgroundOpacity": 0,
  "bottom": "6.91%",
  "id": "IconButton_3B2A622B_34F3_FB70_41A6_C18F4175D785",
  "maxHeight": 55,
  "borderSize": 0,
  "class": "IconButton",
  "minWidth": 1,
  "right": "13.04%",
  "paddingTop": 0,
  "shadow": false
 }
], 
 "mouseWheelEnabled": true,
 "verticalAlign": "top",
 "scrollBarWidth": 10,
 "vrPolyfillScale": 0.5,
 "height": "100%",
 "width": "100%",
 "scripts": {
  "updateMediaLabelFromPlayList": function(playList, htmlText, playListItemStopToDispose){  var changeFunction = function(){ var index = playList.get('selectedIndex'); if(index >= 0){ var beginFunction = function(){ playListItem.unbind('begin', beginFunction); setMediaLabel(index); }; var setMediaLabel = function(index){ var media = playListItem.get('media'); var text = media.get('data'); if(!text) text = media.get('label'); setHtml(text); }; var setHtml = function(text){ if(text !== undefined) { htmlText.set('visible', true); htmlText.set('html', '<div style=\"text-align:left\"><SPAN STYLE=\"color:#FFFFFF;font-size:12px;font-family:Verdana\"><span color=\"white\" font-family=\"Verdana\" font-size=\"12px\">' + text + '</SPAN></div>'); } else { htmlText.set('visible', false); htmlText.set('html', ''); } }; var playListItem = playList.get('items')[index]; if(htmlText.get('html')){ setHtml('Loading...'); playListItem.bind('begin', beginFunction); } else{ setMediaLabel(index); } } }; var disposeFunction = function(){ htmlText.set('html', undefined); playList.unbind('change', changeFunction, this); playListItemStopToDispose.unbind('stop', disposeFunction, this); }; if(playListItemStopToDispose){ playListItemStopToDispose.bind('stop', disposeFunction, this); } playList.bind('change', changeFunction, this); changeFunction(); },
  "fixTogglePlayPauseButton": function(player){  var state = player.get('state'); var button = player.get('buttonPlayPause'); if(typeof button !== 'undefined' && player.get('state') == 'playing'){ button.set('pressed', true); } },
  "shareGoogle": function(url){  window.open('https://plus.google.com/share?url=' + url, '_blank'); },
  "shareFacebook": function(url){  window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, '_blank'); },
  "playGlobalAudioWhilePlay": function(playList, index, audio, endCallback){  var changeFunction = function(event){ if(event.data.previousSelectedIndex == index){ this.stopGlobalAudio(audio); if(isPanorama) { var media = playListItem.get('media'); var audios = media.get('audios'); audios.splice(audios.indexOf(audio), 1); media.set('audios', audios); } playList.unbind('change', changeFunction, this); if(endCallback) endCallback(); } }; var audios = window.currentGlobalAudios; if(audios && audio.get('id') in audios){ audio = audios[audio.get('id')]; if(audio.get('state') != 'playing'){ audio.play(); } return; } playList.bind('change', changeFunction, this); var playListItem = playList.get('items')[index]; var isPanorama = playListItem.get('class') == 'PanoramaPlayListItem'; if(isPanorama) { var media = playListItem.get('media'); var audios = (media.get('audios') || []).slice(); if(audio.get('class') == 'MediaAudio') { var panoramaAudio = this.rootPlayer.createInstance('PanoramaAudio'); panoramaAudio.set('autoplay', false); panoramaAudio.set('audio', audio.get('audio')); panoramaAudio.set('loop', audio.get('loop')); panoramaAudio.set('id', audio.get('id')); audio = panoramaAudio; } audios.push(audio); media.set('audios', audios); } this.playGlobalAudio(audio, endCallback); },
  "startPanoramaWithCamera": function(panorama, camera){  var playLists = this.getByClassName('PlayList'); if(playLists.length == 0) return; var restoreItems = []; for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ var item = items[j]; if(item.get('media') == panorama && item.get('class') == 'PanoramaPlayListItem'){ restoreItems.push({camera: item.get('camera'), item: item}); item.set('camera', camera); } } } if(restoreItems.length > 0) { var restoreCameraOnStop = function(){ for (var i = 0; i < restoreItems.length; i++) { restoreItems[i].item.set('camera', restoreItems[i].camera); } restoreItems[0].item.unbind('stop', restoreCameraOnStop, this); }; restoreItems[0].item.bind('stop', restoreCameraOnStop, this); } },
  "showPopupPanoramaVideoOverlay": function(popupPanoramaOverlay, closeButtonProperties){  var self = this; var showEndFunction = function() { popupPanoramaOverlay.unbind('showEnd', showEndFunction); closeButton.bind('click', hideFunction, this); setCloseButtonPosition(); closeButton.set('visible', true); }; var endFunction = function() { if(!popupPanoramaOverlay.get('loop')) hideFunction(); }; var hideFunction = function() { self.MainViewer.set('toolTipEnabled', true); popupPanoramaOverlay.set('visible', false); closeButton.set('visible', false); closeButton.unbind('click', hideFunction, self); popupPanoramaOverlay.unbind('end', endFunction, self); popupPanoramaOverlay.unbind('hideEnd', hideFunction, self, true); self.resumePlayers(playersPaused, true); self.resumeGlobalAudios(); }; var setCloseButtonPosition = function() { var right = 10; var top = 10; closeButton.set('right', right); closeButton.set('top', top); }; this.MainViewer.set('toolTipEnabled', false); var closeButton = this.closeButtonPopupPanorama; if(closeButtonProperties){ for(var key in closeButtonProperties){ closeButton.set(key, closeButtonProperties[key]); } } var playersPaused = this.pauseCurrentPlayers(true); this.pauseGlobalAudios(); popupPanoramaOverlay.bind('end', endFunction, this, true); popupPanoramaOverlay.bind('showEnd', showEndFunction, this, true); popupPanoramaOverlay.bind('hideEnd', hideFunction, this, true); popupPanoramaOverlay.set('visible', true); },
  "resumePlayers": function(players, onlyResumeCameraIfPanorama){  for(var i = 0; i<players.length; ++i){ var player = players[i]; if(onlyResumeCameraIfPanorama && player.get('class') == 'PanoramaPlayer' && typeof player.get('video') === 'undefined'){ player.resumeCamera(); } else{ player.play(); } } },
  "getActivePlayerWithViewer": function(viewerArea){  var players = this.getByClassName('PanoramaPlayer'); players = players.concat(this.getByClassName('VideoPlayer')); players = players.concat(this.getByClassName('Video360Player')); players = players.concat(this.getByClassName('PhotoAlbumPlayer')); players = players.concat(this.getByClassName('MapPlayer')); var i = players.length; while(i-- > 0){ var player = players[i]; if(player.get('viewerArea') == viewerArea) { var playerClass = player.get('class'); if(playerClass == 'PanoramaPlayer' && (player.get('panorama') != undefined || player.get('video') != undefined)) return player; else if((playerClass == 'VideoPlayer' || playerClass == 'Video360Player') && player.get('video') != undefined) return player; else if(playerClass == 'PhotoAlbumPlayer' && player.get('photoAlbum') != undefined) return player; else if(playerClass == 'MapPlayer' && player.get('map') != undefined) return player; } } return undefined; },
  "shareTwitter": function(url){  window.open('https://twitter.com/intent/tweet?source=webclient&url=' + url, '_blank'); },
  "setComponentVisibility": function(component, visible, applyAt, effect, propertyEffect, ignoreClearTimeout){  this.unregisterKey('visibility_'+component.get('id')); var changeVisibility = function(){ if(effect && propertyEffect){ component.set(propertyEffect, effect); } component.set('visible', visible); if(component.get('class') == 'ViewerArea'){ try{ if(visible) component.restart(); else if(component.get('playbackState') == 'playing') component.pause(); } catch(e){}; } }; var effectTimeoutName = 'effectTimeout_'+component.get('id'); if(!ignoreClearTimeout && window.hasOwnProperty(effectTimeoutName)){ var effectTimeout = window[effectTimeoutName]; if(effectTimeout instanceof Array){ for(var i=0; i<effectTimeout.length; i++){ clearTimeout(effectTimeout[i]) } }else{ clearTimeout(effectTimeout); } delete window[effectTimeoutName]; } else if(visible == component.get('visible') && !ignoreClearTimeout) return; if(applyAt && applyAt > 0){ var effectTimeout = setTimeout(function(){ if(window[effectTimeoutName] instanceof Array) { var arrayTimeoutVal = window[effectTimeoutName]; var index = arrayTimeoutVal.indexOf(effectTimeout); arrayTimeoutVal.splice(index, 1); if(arrayTimeoutVal.length == 0){ delete window[effectTimeoutName]; } }else{ delete window[effectTimeoutName]; } changeVisibility(); }, applyAt); if(window.hasOwnProperty(effectTimeoutName)){ window[effectTimeoutName] = [window[effectTimeoutName], effectTimeout]; }else{ window[effectTimeoutName] = effectTimeout; } } else{ changeVisibility(); } },
  "executeFunctionWhenChange": function(playList, index, endFunction, changeFunction){  var self = this; var endObject = undefined; var changePlayListFunction = function(event){ if(event.data.previousSelectedIndex == index){ if(changeFunction) changeFunction(); if(endFunction && endObject) endObject.unbind('end', endFunction, self); playList.unbind('change', changePlayListFunction, self); } }; if(endFunction){ var playListItem = playList.get('items')[index]; var playListItemClass = playListItem.get('class'); if(playListItemClass == 'PanoramaPlayListItem'){ var camera = playListItem.get('camera'); endObject = camera.get('initialSequence'); } else{ endObject = playListItem.get('media'); } if(endObject){ endObject.bind('end', endFunction, this); } } playList.bind('change', changePlayListFunction, this); },
  "stopGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios){ audio = audios[audio.get('id')]; if(audio) delete audios[audio.get('id')]; } if(audio) audio.stop(); },
  "registerKey": function(key, value){  window[key] = value; },
  "pauseGlobalAudiosWhilePlayItem": function(playList, index, caller){  var audios = window.currentGlobalAudios; if(!audios) return; var resumeFunction = this.resumeGlobalAudios; var endFunction = function(){ if(playList.get('selectedIndex') != index) { resumeFunction(caller); } }; this.pauseGlobalAudios(caller); this.executeFunctionWhenChange(playList, index, endFunction, endFunction); },
  "openLink": function(url, name){  if(url == location.href) { return; } if (name == '_blank' && window && window.process && window.process.versions && window.process.versions['electron']){ if (url.startsWith('/')) { var r = window.location.href.split('/'); r.pop(); url = r.join('/') + url; } var extension = url.split('.').pop().toLowerCase(); if(extension != 'pdf') { var shell = require('electron').shell; shell.openExternal(url); } else { window.open(url, name); } } else { var newWindow = window.open(url, name); newWindow.focus(); } },
  "setMainMediaByIndex": function(index){  if(index >= 0 && index < this.mainPlayList.get('items').length){ this.mainPlayList.set('selectedIndex', index); } },
  "showWindow": function(w, autoCloseMilliSeconds, containsAudio){  if(w.get('visible') == true){ return; } var closeFunction = function(){ clearAutoClose(); this.resumePlayers(playersPaused, !containsAudio); w.unbind('close', closeFunction, this); }; var clearAutoClose = function(){ w.unbind('click', clearAutoClose, this); if(timeoutID != undefined){ clearTimeout(timeoutID); } }; var timeoutID = undefined; if(autoCloseMilliSeconds){ var autoCloseFunction = function(){ w.hide(); }; w.bind('click', clearAutoClose, this); timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds); } var playersPaused = this.pauseCurrentPlayers(!containsAudio); w.bind('close', closeFunction, this); w.show(this, true); },
  "showPopupMedia": function(w, media, playList, popupMaxWidth, popupMaxHeight, autoCloseWhenFinished, containsAudio){  var self = this; var closeFunction = function(){ self.MainViewer.set('toolTipEnabled', true); this.resumePlayers(playersPaused, !containsAudio); if(isVideo) { this.unbind('resize', resizeFunction, this); } w.unbind('close', closeFunction, this); }; var endFunction = function(){ w.hide(); }; var resizeFunction = function(){ var parentWidth = self.get('actualWidth'); var parentHeight = self.get('actualHeight'); var mediaWidth = media.get('width'); var mediaHeight = media.get('height'); var popupMaxWidthNumber = parseFloat(popupMaxWidth) / 100; var popupMaxHeightNumber = parseFloat(popupMaxHeight) / 100; var windowWidth = popupMaxWidthNumber * parentWidth; var windowHeight = popupMaxHeightNumber * parentHeight; var footerHeight = w.get('footerHeight'); var headerHeight = w.get('headerHeight'); if(!headerHeight) { var closeButtonHeight = w.get('closeButtonIconHeight') + w.get('closeButtonPaddingTop') + w.get('closeButtonPaddingBottom'); var titleHeight = w.get('titleFontSize') + w.get('titlePaddingTop') + w.get('titlePaddingBottom'); headerHeight = closeButtonHeight > titleHeight ? closeButtonHeight : titleHeight; headerHeight += w.get('headerPaddingTop') + w.get('headerPaddingBottom'); } if(!footerHeight) { footerHeight = 0; } var contentWindowWidth = windowWidth - w.get('bodyPaddingLeft') - w.get('bodyPaddingRight') - w.get('paddingLeft') - w.get('paddingRight'); var contentWindowHeight = windowHeight - headerHeight - footerHeight - w.get('bodyPaddingTop') - w.get('bodyPaddingBottom') - w.get('paddingTop') - w.get('paddingBottom'); var parentAspectRatio = contentWindowWidth / contentWindowHeight; var mediaAspectRatio = mediaWidth / mediaHeight; if(parentAspectRatio > mediaAspectRatio) { windowWidth = contentWindowHeight * mediaAspectRatio + w.get('bodyPaddingLeft') + w.get('bodyPaddingRight') + w.get('paddingLeft') + w.get('paddingRight'); } else { windowHeight = contentWindowWidth / mediaAspectRatio + headerHeight + footerHeight + w.get('bodyPaddingTop') + w.get('bodyPaddingBottom') + w.get('paddingTop') + w.get('paddingBottom'); } if(windowWidth > parentWidth * popupMaxWidthNumber) { windowWidth = parentWidth * popupMaxWidthNumber; } if(windowHeight > parentHeight * popupMaxHeightNumber) { windowHeight = parentHeight * popupMaxHeightNumber; } w.set('width', windowWidth); w.set('height', windowHeight); w.set('x', (parentWidth - w.get('actualWidth')) * 0.5); w.set('y', (parentHeight - w.get('actualHeight')) * 0.5); }; if(autoCloseWhenFinished){ this.executeFunctionWhenChange(playList, 0, endFunction); } var isVideo = media.get('class') == 'Video'; if(isVideo){ this.bind('resize', resizeFunction, this); resizeFunction(); } else { w.set('width', popupMaxWidth); w.set('height', popupMaxHeight); } this.MainViewer.set('toolTipEnabled', false); var playersPaused = this.pauseCurrentPlayers(!containsAudio); w.bind('close', closeFunction, this); w.show(this, true); },
  "visibleComponentsIfPlayerFlagEnabled": function(components, playerFlag){  var enabled = this.get(playerFlag); for(var i in components){ components[i].set('visible', enabled); } },
  "pauseGlobalAudios": function(caller){  var audios = window.currentGlobalAudios; window.currentGlobalAudiosActionCaller = caller; if(!audios) return; for(var audio in audios){ var a = audios[audio]; if(a.get('state') == 'playing') a.pause(); } },
  "existsKey": function(key){  return key in window; },
  "getPlayListWithMedia": function(media, onlySelected){  var playLists = this.getByClassName('PlayList'); for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; if(onlySelected && playList.get('selectedIndex') == -1) continue; var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ if(items[j].get('media') == media) return playList; } } return undefined; },
  "resumeGlobalAudios": function(caller){  if(window.currentGlobalAudiosActionCaller && window.currentGlobalAudiosActionCaller != caller) return; window.currentGlobalAudiosActionCaller = undefined; var audios = window.currentGlobalAudios; if(!audios) return; for(var audio in audios){ audios[audio].play(); } },
  "getCurrentPlayers": function(){  var players = this.getByClassName('PanoramaPlayer'); players = players.concat(this.getByClassName('VideoPlayer')); players = players.concat(this.getByClassName('Video360Player')); players = players.concat(this.getByClassName('PhotoAlbumPlayer')); return players; },
  "getCurrentPlayerWithMedia": function(media){  var playerClass = undefined; var mediaPropertyName = undefined; switch(media.get('class')) { case 'Panorama': case 'LivePanorama': case 'HDRPanorama': playerClass = 'PanoramaPlayer'; mediaPropertyName = 'panorama'; break; case 'Video360': playerClass = 'PanoramaPlayer'; mediaPropertyName = 'video'; break; case 'PhotoAlbum': playerClass = 'PhotoAlbumPlayer'; mediaPropertyName = 'photoAlbum'; break; case 'Map': playerClass = 'MapPlayer'; mediaPropertyName = 'map'; break; case 'Video': playerClass = 'VideoPlayer'; mediaPropertyName = 'video'; break; }; if(playerClass != undefined) { var players = this.getByClassName(playerClass); for(var i = 0; i<players.length; ++i){ var player = players[i]; if(player.get(mediaPropertyName) == media) { return player; } } } else { return undefined; } },
  "loopAlbum": function(playList, index){  var playListItem = playList.get('items')[index]; var player = playListItem.get('player'); var loopFunction = function(){ player.play(); }; this.executeFunctionWhenChange(playList, index, loopFunction); },
  "getPlayListItems": function(media, player){  var itemClass = (function() { switch(media.get('class')) { case 'Panorama': case 'LivePanorama': case 'HDRPanorama': return 'PanoramaPlayListItem'; case 'Video360': return 'Video360PlayListItem'; case 'PhotoAlbum': return 'PhotoAlbumPlayListItem'; case 'Map': return 'MapPlayListItem'; case 'Video': return 'VideoPlayListItem'; } })(); if (itemClass != undefined) { var items = this.getByClassName(itemClass); for (var i = items.length-1; i>=0; --i) { var item = items[i]; if(item.get('media') != media || (player != undefined && item.get('player') != player)) { items.splice(i, 1); } } return items; } else { return []; } },
  "setStartTimeVideo": function(media, time){  var items = this.getPlayListItems(media); var startTimeBackup = []; var restoreStartTimeFunc = function() { for(var i = 0; i<items.length; ++i){ var item = items[i]; item.set('startTime', startTimeBackup[i]); item.unbind('stop', restoreStartTimeFunc, this); } }; for(var i = 0; i<items.length; ++i) { var item = items[i]; startTimeBackup.push(item.get('startTime')); item.set('startTime', time); item.bind('stop', restoreStartTimeFunc, this); } },
  "updateVideoCues": function(playList, index){  var playListItem = playList.get('items')[index]; var video = playListItem.get('media'); if(video.get('cues').length == 0) return; var player = playListItem.get('player'); var cues = []; var changeFunction = function(){ if(playList.get('selectedIndex') != index){ video.unbind('cueChange', cueChangeFunction, this); playList.unbind('change', changeFunction, this); } }; var cueChangeFunction = function(event){ var activeCues = event.data.activeCues; for(var i = 0, count = cues.length; i<count; ++i){ var cue = cues[i]; if(activeCues.indexOf(cue) == -1 && (cue.get('startTime') > player.get('currentTime') || cue.get('endTime') < player.get('currentTime')+0.5)){ cue.trigger('end'); } } cues = activeCues; }; video.bind('cueChange', cueChangeFunction, this); playList.bind('change', changeFunction, this); },
  "setMainMediaByName": function(name){  var items = this.mainPlayList.get('items'); for(var i = 0; i<items.length; ++i){ if(items[i].get('media').get('label') == name) { this.mainPlayList.set('selectedIndex', i); return; } } },
  "setMediaBehaviour": function(playList, index, mediaDispatcher){  var self = this; var stateChangeFunction = function(event){ if(event.data.state == 'stopped'){ dispose(true); } }; var changeFunction = function(){ var index = playListDispatcher.get('selectedIndex'); if(index != -1){ indexDispatcher = index; dispose(false); } }; var disposeCallback = function(){ dispose(false); }; var dispose = function(forceDispose){ if(!playListDispatcher) return; var media = item.get('media'); if((media.get('class') == 'Video360' || media.get('class') == 'Video') && media.get('loop') == true && !forceDispose) return; playList.set('selectedIndex', -1); if(panoramaSequence && panoramaSequenceIndex != -1){ if(panoramaSequence) { if(panoramaSequenceIndex > 0 && panoramaSequence.get('movements')[panoramaSequenceIndex-1].get('class') == 'TargetPanoramaCameraMovement'){ var initialPosition = camera.get('initialPosition'); var oldYaw = initialPosition.get('yaw'); var oldPitch = initialPosition.get('pitch'); var oldHfov = initialPosition.get('hfov'); var previousMovement = panoramaSequence.get('movements')[panoramaSequenceIndex-1]; initialPosition.set('yaw', previousMovement.get('targetYaw')); initialPosition.set('pitch', previousMovement.get('targetPitch')); initialPosition.set('hfov', previousMovement.get('targetHfov')); var restoreInitialPositionFunction = function(event){ initialPosition.set('yaw', oldYaw); initialPosition.set('pitch', oldPitch); initialPosition.set('hfov', oldHfov); itemDispatcher.unbind('end', restoreInitialPositionFunction, self); }; itemDispatcher.bind('end', restoreInitialPositionFunction, self); } panoramaSequence.set('movementIndex', panoramaSequenceIndex); } } playListDispatcher.set('selectedIndex', indexDispatcher); if(player){ player.unbind('stateChange', stateChangeFunction, self); for(var i = 0; i<buttons.length; ++i) { buttons[i].unbind('click', disposeCallback, self); } } if(sameViewerArea){ if(playList != playListDispatcher) playListDispatcher.unbind('change', changeFunction, self); } else{ viewerArea.set('visible', false); } playListDispatcher = undefined; }; var mediaDispatcherByParam = mediaDispatcher != undefined; if(!mediaDispatcher){ var currentIndex = playList.get('selectedIndex'); var currentPlayer = (currentIndex != -1) ? playList.get('items')[playList.get('selectedIndex')].get('player') : this.getActivePlayerWithViewer(this.MainViewer); if(currentPlayer) { var playerClass = currentPlayer.get('class'); if(playerClass == 'PanoramaPlayer') { mediaDispatcher = currentPlayer.get('panorama'); if(mediaDispatcher == undefined) medisDispatcher = currentPlayer.get('video'); } else if(playerClass == 'VideoPlayer' || playerClass == 'Video360Player') mediaDispatcher = currentPlayer.get('video'); else if(playerClass == 'PhotoAlbumPlayer') mediaDispatcher = currentPlayer.get('photoAlbum'); else if(playerClass == 'MapPlayer') mediaDispatcher = currentPlayer.get('map'); } } var playListDispatcher = mediaDispatcher ? this.getPlayListWithMedia(mediaDispatcher, true) : undefined; if(!playListDispatcher){ playList.set('selectedIndex', index); return; } var indexDispatcher = playListDispatcher.get('selectedIndex'); if(playList.get('selectedIndex') == index || indexDispatcher == -1){ return; } var item = playList.get('items')[index]; var itemDispatcher = playListDispatcher.get('items')[indexDispatcher]; var viewerArea = item.get('player').get('viewerArea'); var sameViewerArea = viewerArea == itemDispatcher.get('player').get('viewerArea'); if(sameViewerArea){ if(playList != playListDispatcher){ playListDispatcher.set('selectedIndex', -1); playListDispatcher.bind('change', changeFunction, this); } } else{ viewerArea.set('visible', true); } var panoramaSequenceIndex = -1; var panoramaSequence = undefined; var camera = itemDispatcher.get('camera'); if(camera){ panoramaSequence = camera.get('initialSequence'); if(panoramaSequence) { panoramaSequenceIndex = panoramaSequence.get('movementIndex'); } } playList.set('selectedIndex', index); var player = undefined; var buttons = []; if(item.get('player') != itemDispatcher.get('player') || !mediaDispatcherByParam){ player = item.get('player'); if(player.get('class') == 'PanoramaPlayer' && item.get('media').get('class') != 'Video360') { var addButtons = function(property){ var value = player.get(property); if(Array.isArray(value)) buttons = buttons.concat(value); else buttons.push(value); }; addButtons('buttonStop'); for(var i = 0; i<buttons.length; ++i) { buttons[i].bind('click', disposeCallback, this); } } else { setTimeout(function(){player.bind('stateChange', stateChangeFunction, self)}, 100); } } this.executeFunctionWhenChange(playList, index, disposeCallback); },
  "pauseGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios){ audio = audios[audio.get('id')]; } if(audio.get('state') == 'playing') audio.pause(); },
  "showPopupImage": function(image, toggleImage, customWidth, customHeight, showEffect, hideEffect, closeButtonProperties, autoCloseMilliSeconds, audio, stopBackgroundAudio, loadedCallback, hideCallback){  var self = this; var closed = false; var playerClickFunction = function() { zoomImage.unbind('loaded', loadedFunction, self); hideFunction(); }; var clearAutoClose = function(){ zoomImage.unbind('click', clearAutoClose, this); if(timeoutID != undefined){ clearTimeout(timeoutID); } }; var loadedFunction = function(){ self.unbind('click', playerClickFunction, self); veil.set('visible', true); setCloseButtonPosition(); closeButton.set('visible', true); zoomImage.unbind('loaded', loadedFunction, this); zoomImage.bind('userInteractionStart', userInteractionStartFunction, this); zoomImage.bind('userInteractionEnd', userInteractionEndFunction, this); timeoutID = setTimeout(timeoutFunction, 200); }; var timeoutFunction = function(){ timeoutID = undefined; if(autoCloseMilliSeconds){ var autoCloseFunction = function(){ hideFunction(); }; zoomImage.bind('click', clearAutoClose, this); timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds); } zoomImage.bind('backgroundClick', hideFunction, this); if(toggleImage) { zoomImage.bind('click', toggleFunction, this); zoomImage.set('imageCursor', 'hand'); } closeButton.bind('click', hideFunction, this); if(loadedCallback) loadedCallback(); }; var hideFunction = function() { self.MainViewer.set('toolTipEnabled', true); closed = true; if(timeoutID) clearTimeout(timeoutID); if(autoCloseMilliSeconds) clearAutoClose(); if(hideCallback) hideCallback(); zoomImage.set('visible', false); if(hideEffect && hideEffect.get('duration') > 0){ hideEffect.bind('end', endEffectFunction, this); } else{ zoomImage.set('image', null); } closeButton.set('visible', false); veil.set('visible', false); self.unbind('click', playerClickFunction, self); zoomImage.unbind('backgroundClick', hideFunction, this); zoomImage.unbind('userInteractionStart', userInteractionStartFunction, this); zoomImage.unbind('userInteractionEnd', userInteractionEndFunction, this, true); if(toggleImage) { zoomImage.unbind('click', toggleFunction, this); zoomImage.set('cursor', 'default'); } closeButton.unbind('click', hideFunction, this); self.resumePlayers(playersPaused, audio == null || stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ self.resumeGlobalAudios(); } self.stopGlobalAudio(audio); } }; var endEffectFunction = function() { zoomImage.set('image', null); hideEffect.unbind('end', endEffectFunction, this); }; var toggleFunction = function() { zoomImage.set('image', isToggleVisible() ? image : toggleImage); }; var isToggleVisible = function() { return zoomImage.get('image') == toggleImage; }; var setCloseButtonPosition = function() { var right = zoomImage.get('actualWidth') - zoomImage.get('imageLeft') - zoomImage.get('imageWidth') + 10; var top = zoomImage.get('imageTop') + 10; if(right < 10) right = 10; if(top < 10) top = 10; closeButton.set('right', right); closeButton.set('top', top); }; var userInteractionStartFunction = function() { if(timeoutUserInteractionID){ clearTimeout(timeoutUserInteractionID); timeoutUserInteractionID = undefined; } else{ closeButton.set('visible', false); } }; var userInteractionEndFunction = function() { if(!closed){ timeoutUserInteractionID = setTimeout(userInteractionTimeoutFunction, 300); } }; var userInteractionTimeoutFunction = function() { timeoutUserInteractionID = undefined; closeButton.set('visible', true); setCloseButtonPosition(); }; this.MainViewer.set('toolTipEnabled', false); var veil = this.veilPopupPanorama; var zoomImage = this.zoomImagePopupPanorama; var closeButton = this.closeButtonPopupPanorama; if(closeButtonProperties){ for(var key in closeButtonProperties){ closeButton.set(key, closeButtonProperties[key]); } } var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio); if(audio){ if(stopBackgroundAudio){ this.pauseGlobalAudios(); } this.playGlobalAudio(audio); } var timeoutID = undefined; var timeoutUserInteractionID = undefined; zoomImage.bind('loaded', loadedFunction, this); setTimeout(function(){ self.bind('click', playerClickFunction, self, false); }, 0); zoomImage.set('image', image); zoomImage.set('customWidth', customWidth); zoomImage.set('customHeight', customHeight); zoomImage.set('showEffect', showEffect); zoomImage.set('hideEffect', hideEffect); zoomImage.set('visible', true); return zoomImage; },
  "isCardboardViewMode": function(){  var players = this.getByClassName('PanoramaPlayer'); return players.length > 0 && players[0].get('viewMode') == 'cardboard'; },
  "getKey": function(key){  return window[key]; },
  "autotriggerAtStart": function(player, callback){  var stateChangeFunction = function(event){ if(event.data.state == 'playing'){ callback(); player.unbind('stateChange', stateChangeFunction, this); } }; player.bind('stateChange', stateChangeFunction, this); },
  "changeBackgroundWhilePlay": function(playList, index, color){  var changeFunction = function(event){ if(event.data.previousSelectedIndex == index){ playList.unbind('change', changeFunction, this); if((color == viewerArea.get('backgroundColor')) && (colorRatios == viewerArea.get('backgroundColorRatios'))){ viewerArea.set('backgroundColor', backgroundColorBackup); viewerArea.set('backgroundColorRatios', backgroundColorRatiosBackup); } } }; var playListItem = playList.get('items')[index]; var player = playListItem.get('player'); var viewerArea = player.get('viewerArea'); var backgroundColorBackup = viewerArea.get('backgroundColor'); var backgroundColorRatiosBackup = viewerArea.get('backgroundColorRatios'); var colorRatios = [0]; if((color != backgroundColorBackup) || (colorRatios != backgroundColorRatiosBackup)){ viewerArea.set('backgroundColor', color); viewerArea.set('backgroundColorRatios', colorRatios); playList.bind('change', changeFunction, this); } },
  "setEndToItemIndex": function(playList, fromIndex, toIndex){  var endFunction = function(){ if(playList.get('selectedIndex') == fromIndex) playList.set('selectedIndex', toIndex); }; this.executeFunctionWhenChange(playList, fromIndex, endFunction); },
  "setMapLocation": function(panoramaPlayListItem, mapPlayer){  var resetFunction = function(){ panoramaPlayListItem.unbind('stop', resetFunction, this); player.set('mapPlayer', null); }; panoramaPlayListItem.bind('stop', resetFunction, this); var player = panoramaPlayListItem.get('player'); player.set('mapPlayer', mapPlayer); },
  "pauseCurrentPlayers": function(onlyPauseCameraIfPanorama){  var players = this.getCurrentPlayers(); var i = players.length; while(i-- > 0){ var player = players[i]; if(player.get('state') == 'playing') { if(onlyPauseCameraIfPanorama && player.get('class') == 'PanoramaPlayer' && typeof player.get('video') === 'undefined'){ player.pauseCamera(); } else if(player.get('state') == 'playing') { player.pause(); } } else { players.splice(i, 1); } } return players; },
  "playGlobalAudio": function(audio, endCallback){  var endFunction = function(){ audio.unbind('end', endFunction, this); this.stopGlobalAudio(audio); if(endCallback) endCallback(); }; audio = this.getGlobalAudio(audio); var audios = window.currentGlobalAudios; if(!audios){ audios = window.currentGlobalAudios = {}; } audios[audio.get('id')] = audio; if(audio.get('state') == 'playing'){ return; } if(!audio.get('loop')){ audio.bind('end', endFunction, this); } audio.play(); },
  "setStartTimeVideoSync": function(media, player){  this.setStartTimeVideo(media, player.get('currentTime')); },
  "unregisterKey": function(key){  delete window[key]; },
  "showComponentsWhileMouseOver": function(parentComponent, components, durationVisibleWhileOut){  var setVisibility = function(visible){ for(var i = 0, length = components.length; i<length; i++){ var component = components[i]; if(component.get('class') == 'HTMLText' && (component.get('text') == '' || component.get('text') == undefined)) { continue; } component.set('visible', visible); } }; if (this.rootPlayer.get('touchEnabled') == true){ setVisibility(true); } else { var timeoutID = -1; var rollOverFunction = function(){ setVisibility(true); if(timeoutID >= 0) clearTimeout(timeoutID); parentComponent.unbind('rollOver', rollOverFunction, this); parentComponent.bind('rollOut', rollOutFunction, this); }; var rollOutFunction = function(){ var timeoutFunction = function(){ setVisibility(false); parentComponent.unbind('rollOver', rollOverFunction, this); }; parentComponent.unbind('rollOut', rollOutFunction, this); parentComponent.bind('rollOver', rollOverFunction, this); timeoutID = setTimeout(timeoutFunction, durationVisibleWhileOut); }; parentComponent.bind('rollOver', rollOverFunction, this); } },
  "playAudioList": function(audios){  if(audios.length == 0) return; var currentAudioCount = -1; var currentAudio; var playGlobalAudioFunction = this.playGlobalAudio; var playNext = function(){ if(++currentAudioCount >= audios.length) currentAudioCount = 0; currentAudio = audios[currentAudioCount]; playGlobalAudioFunction(currentAudio, playNext); }; playNext(); },
  "syncPlaylists": function(playLists){  var changeToMedia = function(media, playListDispatched){ for(var i = 0, count = playLists.length; i<count; ++i){ var playList = playLists[i]; if(playList != playListDispatched){ var items = playList.get('items'); for(var j = 0, countJ = items.length; j<countJ; ++j){ if(items[j].get('media') == media){ if(playList.get('selectedIndex') != j){ playList.set('selectedIndex', j); } break; } } } } }; var changeFunction = function(event){ var playListDispatched = event.source; var selectedIndex = playListDispatched.get('selectedIndex'); if(selectedIndex < 0) return; var media = playListDispatched.get('items')[selectedIndex].get('media'); changeToMedia(media, playListDispatched); }; var mapPlayerChangeFunction = function(event){ var panoramaMapLocation = event.source.get('panoramaMapLocation'); if(panoramaMapLocation){ var map = panoramaMapLocation.get('map'); changeToMedia(map); } }; for(var i = 0, count = playLists.length; i<count; ++i){ playLists[i].bind('change', changeFunction, this); } var mapPlayers = this.getByClassName('MapPlayer'); for(var i = 0, count = mapPlayers.length; i<count; ++i){ mapPlayers[i].bind('panoramaMapLocation_change', mapPlayerChangeFunction, this); } },
  "getGlobalAudio": function(audio){  var audios = window.currentGlobalAudios; if(audios != undefined && audio.get('id') in audios){ audio = audios[audio.get('id')]; } return audio; },
  "loadFromCurrentMediaPlayList": function(playList, delta){  var currentIndex = playList.get('selectedIndex'); var totalItems = playList.get('items').length; var newIndex = (currentIndex + delta) % totalItems; while(newIndex < 0){ newIndex = totalItems + newIndex; }; if(currentIndex != newIndex){ playList.set('selectedIndex', newIndex); }; }
 },
 "borderRadius": 0,
 "gap": 10,
 "backgroundPreloadEnabled": true,
 "start": "this.visibleComponentsIfPlayerFlagEnabled([this.IconButton_3B2A622B_34F3_FB70_41A6_C18F4175D785], 'cardboardAvailable'); this.mainPlayList.set('selectedIndex', 0); if(!this.get('fullscreenAvailable')) { [this.IconButton_3B148E12_34F0_AB50_41C3_18F7168B2FBA].forEach(function(component) { component.set('visible', false); }) }",
 "horizontalAlign": "left",
 "minHeight": 20,
 "contentOpaque": false,
 "paddingLeft": 0,
 "scrollBarColor": "#000000",
 "paddingBottom": 0,
 "paddingRight": 0,
 "scrollBarOpacity": 0.5,
 "layout": "absolute",
 "overflow": "visible",
 "buttonToggleFullscreen": "this.IconButton_3B148E12_34F0_AB50_41C3_18F7168B2FBA",
 "id": "rootPlayer",
 "borderSize": 0,
 "class": "Player",
 "scrollBarVisible": "rollOver",
 "paddingTop": 0,
 "shadow": false,
 "minWidth": 20,
 "scrollBarMargin": 2
})