$(function () {
    function after_form_submitted(data) {
        console.log(data);
        if (data.result == 'success') {
            $('form.form-action').hide();
            $('.form-success_message').show();
            $('.form-error_message').hide();
            $('form.form-action input').removeClass('required');
        } else {
            $('.form-error_message').append('<ul></ul>');

            jQuery.each(data.errors, function (key, val) {

                //$('#error_message ul').append('<li>'+key+': é um campo obrigatório'+'</li>');

                if (key == 'telefone') {
                    $('form.form-action .telefone').addClass('required');
                }
                if (key == 'nome') {
                    $('form.form-action .name').addClass('required');
                }
                if (key == 'e-mail') {
                    $('form.form-action .email').addClass('required');
                }
            });
            $('.form-success_message').hide();
            $('.form-error_message').show();

            //reverse the response on the button
            $('button[type="button"]', $form).each(function () {
                $btn = $(this);
                label = $btn.prop('orig_label');
                if (label) {
                    $btn.prop('type', 'submit');
                    $btn.text(label);
                    $btn.prop('orig_label', '');
                }
            });

        } //else
    }

    $('form.form-action').submit(function (e) {
        e.preventDefault();

        $form = $(this);
        //show some response on the button
        $('button[type="submit"]', $form).each(function () {
            $btn = $(this);
            $btn.prop('type', 'button');
            $btn.prop('orig_label', $btn.text());
            $btn.text('Enviando...');
        });


        $.ajax({
            type: "POST",
            url: 'handler.php',
            data: $form.serialize(),
            success: after_form_submitted,
            dataType: 'json'
        });

    });
});