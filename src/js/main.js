$(function(){
    var menuBar = $(".section-full.menu-bar"),
        body = $("html, body"),
        $localTop = $('.section-full.localizacao'),
        $botao = $('.menu-bar .menu__links li'),
        $link = $('.menu-bar .menu__links li a');

    $(window).scroll(function(){ 
        var stickyTop = $localTop.offset().top - 200,
            $footerBar = $('.footer__bar'),
            cur_pos = $(this).scrollTop(),
            $section = $('section.module');

        if( $(this).scrollTop() >= stickyTop ){
            menuBar.addClass('menu-bar--scrolled');
            $footerBar.removeClass('footer__bar--scrolled--top');             
        }else{
            menuBar.removeClass('menu-bar--scrolled');   
            $footerBar.addClass('footer__bar--scrolled--top');
        }

        $section.each(function () {
            var top = $(this).offset().top - 202,
                bottom = top + $(this).outerHeight();

            if (cur_pos >= top) {
                menuBar.find('a').removeClass('active');
                $section.removeClass('active');

                $(this).addClass('active');
                menuBar.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');
            }        
        });
    });
    

    $link.on('click', function () {
        var $a = $(this),
            id = $a.attr('href');

            if( $(window).width() <= 991){
                espaco = 0;
            }else{
                espaco = 100;
            }

        $('html, body').animate({
            scrollTop: $(id).offset().top - menuBar.height() - espaco
        }, 1500);

        $(this).closest('ul').find('a').removeClass('active');
        $(this).addClass('active');

        return false;
    });

});

/** animacao bar footer 
$(function(){
    $(window).scroll(function(){
        var $footerBar = $('.footer__bar');

        if($(window).scrollTop() + $(window).height() > $(document).height() - 310) {
            $footerBar.removeClass('footer__bar--scrolled');
        }else{
            $footerBar.addClass('footer__bar--scrolled');
        }
    });
})
**/

$(function(){
    var $imputs = $('.contato__formulario form input');

    $imputs.focus(function(){
        $(this).val("");
    })
    var value=$($imputs).val();

    $imputs.on('blur', function() {
        
        if($(this).val()=="") {
            $(this).val(value);
        }
    });
})

//funcionamento menu mobile
$(function(){
    var $btn = $('.btn-mobile, .menu-bar .menu__links li a'),
        $menu = $('.menu-bar .menu__links');
        
    $btn.click(function(){
        $menu.toggleClass('menu__links--open');
    })
})

//ordenacao de diferenciais mobile
$(function(){
    $(window).ready(function(){

        var $diferencialUm = $('.diferencial__box--one'),
            $diferencialDois = $('.diferencial__box--two'),
            $titleDif = $('.section-full.diferencial .title-default');

        if( $(window).width() <= 991 ){
            $diferencialDois.insertAfter($diferencialUm);
            $titleDif.insertBefore($diferencialUm);
        }

    })
})

//modal mapa
$(function(){
    var $btnModal = $('.open-modal'),
        $modal = $('.modal'),
        $close = $('.modal .close');

    $btnModal.click(function(){
        var btnData = $(this).attr('data-modal');
        if( btnData ==  $modal.attr('id') ){
            $modal.addClass('modal--open');

            $('body').addClass('no-scroll');
        }
    });

    $close.click(function(){
        $(this).closest('.modal').removeClass('modal--open');
        $('body').removeClass('no-scroll');
    });
})