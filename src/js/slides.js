$(document).ready(function() {
  var $sync1 = $("#sliderLazer"),
      $sync2 = $("#listLazer"),
      flag = false,
      duration = 300;
  
  $sync1
    .owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: true,
        dots: true,
        navText: [
          '<i class="icon-arrow-left" aria-hidden="true"></i>',
          '<i class="icon-arrow-right" aria-hidden="true"></i>'
        ]
    })
    .on('changed.owl.carousel', function (e) {
      var syncedPosition = syncPosition(e.item.index);
  
      if ( syncedPosition != "stayStill" ) {
          $sync2.trigger('to.owl.carousel', [syncedPosition, duration, true]);
      }
    });
  
  $sync2
    .on('initialized.owl.carousel', function() { //must go before owl carousel initialization
        addClassCurrent( 0 );
    })
    .owlCarousel({ //owl carousel init
        items: 7
    })
    .on('click', '.owl-item', function () {
        $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
    });
  
  function addClassCurrent( index ) {
    $sync2
        .find(".owl-item")
        .removeClass("current")
        .eq( index ).addClass("current");
  }
  
  function syncPosition( index ) {
  
    addClassCurrent( index );
    var itemsNo = $sync2.find(".owl-item").length; //total items
    var visibleItemsNo = $sync2.find(".owl-item.active").length; //visible items
  
    if (itemsNo === visibleItemsNo) {
        return "stayStill";
    }
  
    var visibleCurrentIndex = $sync2.find(".owl-item.active").index( $sync2.find(".owl-item.current") );
  
    if (visibleCurrentIndex == 0 && index != 0) {
            return index - 1;
    }
  
    if (visibleCurrentIndex == (visibleItemsNo - 1) && index != (itemsNo - 1)) {
            return index - visibleItemsNo + 2;
    }
  
    return "stayStill";
  }

})

$(document).ready(function() {
  var $sync1 = $("#sliderPrivativo"),
      $sync2 = $("#listPrivativo"),
      flag = false,
      duration = 300;
  
  $sync1
    .owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: true,
        dots: true,
        navText: [
          '<i class="icon-arrow-left" aria-hidden="true"></i>',
          '<i class="icon-arrow-right" aria-hidden="true"></i>'
        ]
    })
    .on('changed.owl.carousel', function (e) {
      var syncedPosition = syncPosition(e.item.index);
  
      if ( syncedPosition != "stayStill" ) {
          $sync2.trigger('to.owl.carousel', [syncedPosition, duration, true]);
      }
    });
  
  $sync2
    .on('initialized.owl.carousel', function() { //must go before owl carousel initialization
        addClassCurrent( 0 );
    })
    .owlCarousel({ //owl carousel init
        items: 4
    })
    .on('click', '.owl-item', function () {
        $sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
    });
  
  function addClassCurrent( index ) {
    $sync2
        .find(".owl-item")
        .removeClass("current")
        .eq( index ).addClass("current");
  }
  
  function syncPosition( index ) {
  
    addClassCurrent( index );
    var itemsNo = $sync2.find(".owl-item").length; //total items
    var visibleItemsNo = $sync2.find(".owl-item.active").length; //visible items
  
    if (itemsNo === visibleItemsNo) {
        return "stayStill";
    }
  
    var visibleCurrentIndex = $sync2.find(".owl-item.active").index( $sync2.find(".owl-item.current") );
  
    if (visibleCurrentIndex == 0 && index != 0) {
            return index - 1;
    }
  
    if (visibleCurrentIndex == (visibleItemsNo - 1) && index != (itemsNo - 1)) {
            return index - visibleItemsNo + 2;
    }
  
    return "stayStill";
  }

})

var $sliderPlanta = $('#slidePlanta');

$sliderPlanta.owlCarousel({
  items: 1,
  loop:false,
  margin:10,
  nav:true,
  dots: true,
  autoHeight: true,
  navText: [
    '<i class="icon-arrow-left" aria-hidden="true"></i>',
    '<i class="icon-arrow-right" aria-hidden="true"></i>'
  ]
})

$sliderPlanta.find('.owl-nav button').addClass('btn btn--gradient');

