var gulp = require('gulp');
var browserSync = require("browser-sync").create();
var nunjucks = require('gulp-nunjucks-render');
var autoprefix = require("gulp-autoprefixer");
var sass = require('gulp-sass');
var cleanCss = require("gulp-clean-css");
var plumber = require('gulp-plumber');
var rename = require("gulp-rename");
var sequence = require("gulp-sequence");
var concat = require("gulp-concat");
var newer = require("gulp-newer");
var $ = require('gulp-load-plugins')();

gulp.task("img", () =>
  gulp
    .src("src/image/**/*")
    .pipe(newer("dist/image"))
    .pipe(gulp.dest("dist/image"))
);

gulp.task("form", () =>
  gulp
    .src([
      "src/templates/**/*.php"
    ])
    .pipe(newer("dist"))
    .pipe(gulp.dest("dist"))
);

gulp.task("tour", () =>
  gulp
    .src("src/tour-360/**/*")
    .pipe(newer("dist/tour-360"))
    .pipe(gulp.dest("dist/tour-360"))
);


gulp.task("html", () =>
  gulp
    .src("./src/templates/**.html")
    .pipe(nunjucks({ path: ["./src/templates/"] }))
    .pipe(gulp.dest("./dist/"))
    .pipe(browserSync.stream())
);

gulp.task("css", () =>
  gulp
    .src("src/scss/style.scss")
    .pipe(plumber())
    .pipe(autoprefix())
    .pipe(sass())
    .pipe(cleanCss({ level: 2 }))
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("dist/css"))
    .pipe(browserSync.stream())
);

gulp.task('icons', function () {
  return gulp.src('config.json')
    .pipe($.fontello())
    .pipe(gulp.dest('dist/css/fontello'))
});
  
gulp.task("fonts", () =>
  gulp.src("./src/scss/fonts/*.*").pipe(gulp.dest("./dist/css/fonts"))
);  

gulp.task("js", () =>
  gulp
    .src([
      "./node_modules/jquery/dist/jquery.min.js",
      "./node_modules/owl.carousel/dist/owl.carousel.min.js",
      "src/js/**.js"  
    ])
    .pipe(concat("scripts.js"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("dist/js"))
    .pipe(browserSync.stream())
);

gulp.task("serve", () => {
  browserSync.init({
    server: {
      baseDir: "./dist"
    },
    port: 3000
});

gulp.watch("./src/templates/**/*.html", ["html"]);
gulp.watch("./src/templates/form/**/*.html", ["form"]);
gulp.watch(["./src/scss/**/*.scss"], ["css"]);
gulp.watch("config.json", ["icons", "css"]);
gulp.watch(["src/js/**.js"], ["js"]);
gulp.watch("src/image/**/*", ["img"]);
});

gulp.task(
    "default",
    sequence(
      "html",
      "form",
      "css",
      "js",
      "img",
      "tour",
      "fonts",
      "icons",
      "serve"
    )
);